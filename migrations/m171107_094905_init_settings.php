<?php

use yii\base\InvalidConfigException;
use yii\db\Migration;

class m171107_094905_init_settings extends Migration
{
    /**
     * @var \azbuco\settings\SettingsManager
     */
    protected $_settingsManager;

    public function init()
    {
        parent::init();

        try {
            $this->_settingsManager = Yii::$app->settings;
        } catch (Exception $exc) {
            throw new InvalidConfigException('You should configure "settings" component to use database before executing this migration.');
        }
    }

    public function up()
    {

        $this->createTable($this->getTable(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'key' => $this->string(255)->notNull(),
            'value' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        $this->createIndex('idx-user_id-key', $this->getTable(), ['user_id', 'key'], true);
    }

    public function down()
    {
        $this->dropTable('settings');
        return false;
    }

    protected function getTable()
    {
        $modelClass = $this->_settingsManager->modelClass;
        return $modelClass::tableName();
    }
}
