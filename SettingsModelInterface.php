<?php

namespace azbuco\settings;

interface SettingsModelInterface
{
    /**
     * Returns the key attribute
     */
    public function getKey();

    /**
     * Returns the value attribute
     */
    public function getValue();

    /**
     * Returns all setting models for the user
     * @param mixed $user_id
     * @return SettingsModelInterface[]
     */
    public static function findUserSettings($user_id);

    /**
     * Returns all global setting models
     * @return SettingsModelInterface[]
     */
    public static function findGlobalSettings();

    /**
     * Returns a model for the specified user and key
     * @param string $key
     * @param mixed $user_id
     * @return SettingsModelInterface
     */
    public static function findModel($key, $user_id);
    
    /**
     * Save the key value pairs for the specified user
     * @param array $values as key-value pairs
     * @param mixed $user_id
     */
    public static function saveValues($values, $user_id);
}
