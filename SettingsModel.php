<?php

namespace azbuco\settings;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the core model class for table "settings".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property integer $user_id
 * @property string $created_at
 * @property string $updated_at
 */
class SettingsModel extends ActiveRecord implements SettingsModelInterface
{
    public static $GLOBAL_USER_ID = 0;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'string'],
            [['user_id'], 'integer'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['key'], 'string', 'max' => 255],
            [['key', 'user_id'], 'unique', 'targetAttribute' => ['key', 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Kulcs',
            'value' => 'Érték',
            'user_id' => 'Felhasznalo ID',
            'created_at' => 'Letrehozva',
            'updated_at' => 'Modositva',
            'created_by' => 'Létrehozta',
            'updated_by' => 'Módosította',
        ];
    }

    public static function findModel($key, $user_id)
    {
        return static::findOne(['key' => $key, 'user_id' => $user_id]);
    }

    public static function findGlobalSettings()
    {
        return static::findAll(['user_id' => self::$GLOBAL_USER_ID]);
    }

    public static function findUserSettings($user_id)
    {
        return static::findAll(['user_id' => $user_id]);
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public static function saveValues($values, $user_id)
    {
        $transaction = Yii::$app->db->beginTransaction();

        foreach ($values as $key => $value) {
            $model = static::findModel($key, $user_id);
            
            if ($model === null) {
                $model = new self;
                $model->key = $key;
                $model->user_id = $user_id;
            }
            
            $model->value = $value;

            if (!$model->save()) {
                $transaction->rollback();
                Yii::error("Unable to save settings with: \n - user_id: " . $model->user_id . "\n - key: " . $model->key, "\n - value: \n --- \n" . $model->value . "\n --- \n");
                return false;
            }

            $global = static::findModel($key, self::$GLOBAL_USER_ID);
            
            if ($global !== null) {
                continue;
            }
            
            $global = new self;
            $global->key = $key;
            $global->user_id = self::$GLOBAL_USER_ID;
            $global->value = $value;
            
            if (!$global->save()) {
                $transaction->rollback();
                Yii::error("Unable to save global settings with: \n - key: " . $model->key, "\n - value: \n --- \n" . $model->value . "\n --- \n");
                return false;
            }
        }

        $transaction->commit();
        return true;
    }
}
