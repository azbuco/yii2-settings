<?php

namespace azbuco\settings;

use yii\base\InvalidArgumentException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the core model class for table "user_settings". 
 * 
 * @property integer $id
 * @property string $value
 * @property string $created_at
 * @property string $updated_at
 * @property integer $user_id
 * @property string $settings_key
 * 
 * @property Settings $settingsKey
 */
class UserSettings extends ActiveRecord {

    /**
     * @inheritdoc 
     */
    public static function tableName()
    {
        return 'user_settings';
    }

    /**
     * @inheritdoc 
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'settings_key'], 'required'],
            [['user_id'], 'integer'],
            [['settings_key'], 'string', 'max' => 255],
            [['user_id', 'settings_key'], 'unique', 'targetAttribute' => ['user_id', 'settings_key'], 'message' => 'The combination of User ID and Settings Key has already been taken.'],
            [['settings_key'], 'exist', 'skipOnError' => true, 'targetClass' => Settings::className(), 'targetAttribute' => ['settings_key' => 'key']],
        ];
    }

    /**
     * @inheritdoc 
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
            'settings_key' => 'Settings Key',
        ];
    }

    /**
     * @return ActiveQuery 
     */
    public function getSettingsKey()
    {
        return $this->hasOne(Settings::className(), ['key' => 'settings_key']);
    }

    public static function deleteSetting($user_id, $key)
    {
        return self::deleteAll([
            'user_id' => $user_id,
            'settings_key' => $key,
        ]);
    }

    public static function deleteSettings($user_id)
    {
        return self::deleteAll([
            'user_id' => $user_id,
        ]);
    }

    public static function getSetting($user_id, $key)
    {
        $setting = Settings::findOne($key);
        if (!$setting) {
            throw new InvalidArgumentException('Invalid key "' . $key . '" for setting.');
        }

        if (!$setting->public) {
            return $setting->value;
        }

        $userSetting = UserSettings::findOne([
            'user_id' => $user_id,
            'settings_key' => $key
        ]);

        return $userSetting ? $userSetting->value : $setting->value;
    }

    public static function getSettings($user_id)
    {
        $settings = Settings::find()
        ->select(['`key` AS key', 'COALESCE(`user_settings`.`value`, `settings`.`value`) AS value'])
        ->leftJoin('user_settings', [
            '`user_settings`.`settings_key`' => new Expression('`settings`.`key`'),
            '`user_settings`.`user_id`' => $user_id,
            '`settings`.`public`' => 1,
        ])
        ->asArray()
        ->all();

        return ArrayHelper::map($settings, 'key', 'value');
    }

    public static function saveSetting($user_id, $key, $value)
    {
        $setting = Settings::findOne($key);
        if (!$setting) {
            throw new InvalidArgumentException('The key "' . $key . '" is invalid.');
        }

        if (!$setting->public) {
            throw new InvalidArgumentException('The key "' . $key . '" is private.');
        }

        $userSetting = UserSettings::findOne([
            'user_id' => $user_id,
            'settings_key' => $key,
        ]);

        if ($userSetting === null) {
            $userSetting = new UserSettings();
            $userSetting->user_id = $user_id;
            $userSetting->settings_key = $setting->key;
        }

        $userSetting->value = $value;
        return $userSetting->save();
    }

    public static function saveSettings($user_id, $values)
    {
        $success = true;
        foreach($values as $key => $value) {
            $success = $success && self::saveSetting($user_id, $key, $value);
        }
        return $success;
    }

}
