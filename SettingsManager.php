<?php

namespace azbuco\settings;

use Yii;
use yii\base\Component;

class SettingsManager extends Component {

    /**
     * @var string model class name instance of SettingsModelInterface
     */
    public $modelClass = 'azbuco\settings\UserSettings';
    
    /**
     * @var UserSettingsInterface
     */
    protected $_model;

    public function init()
    {
        $this->_model = new $this->modelClass;
        parent::init();
    }

    public function get($key)
    {
        return $this->_model->getSetting(Yii::$app->user->id, $key);
    }

    public function getSettings()
    {
        return $this->_model->getSettings(Yii::$app->user->id);
    }

    public function save($key, $value)
    {
        if (Yii::$app->user->id === null) {
            throw new \yii\base\InvalidArgumentException('Unable to save unauthenticated user\'s setting');
        }
        
        return $this->_model->saveSetting(Yii::$app->user->id, $key, $value);
    }
    
    public function saveSettings($values)
    {
        if (Yii::$app->user->id === null) {
            throw new \yii\base\InvalidArgumentException('Unable to save unauthenticated user\'s setting');
        }
        
        return $this->_model->saveSettings(Yii::$app->user->id, $values);
    }

}
