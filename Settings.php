<?php

namespace azbuco\settings;

use Yii; 

/** 
 * This is the core model class for table "settings". 
 * 
 * @property string $key
 * @property string $value
 * @property integer $public
 * 
 * @property UserSettings[] $userSettings
 */ 
class Settings extends \yii\db\ActiveRecord
{ 
    /** 
     * @inheritdoc 
     */ 
    public static function tableName() 
    { 
        return 'settings'; 
    } 

    /** 
     * @inheritdoc 
     */ 
    public function rules() 
    { 
        return [
            [['key'], 'required'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 255],
            [['public'], 'string', 'max' => 1],
        ]; 
    } 

    /** 
     * @inheritdoc 
     */ 
    public function attributeLabels() 
    { 
        return [ 
            'key' => 'Key',
            'value' => 'Value',
            'public' => 'Public',
        ]; 
    } 

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getUserSettings() 
    { 
        return $this->hasMany(UserSettings::className(), ['settings_key' => 'key']);
    }

} 