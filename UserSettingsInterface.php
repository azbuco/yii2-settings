<?php

namespace azbuco\settings;

interface UserSettingsInterface {

    /**
     * Returns a setting for the user, with fallback to the default setting
     * @param mixed $user_id
     * @param string $key
     * @return mixed
     */
    public static function getSetting($user_id, $key);

    /**
     * Returns all settings for the user, with fallback to the default settings
     * @param mixed $user_id
     * @return array
     */
    public static function getSettings($user_id);

    /**
     * Saves a user setting
     * @param mixed $user_id
     * @param string $key
     * @param mixed $value
     * @return boolean whether the save is succesfull
     */
    public static function saveSetting($user_id, $key, $value);

    /**
     * Saves some user setting
     * @param mixed $user_id
     * @param array $values array of settings as key => value
     * @return boolean whether the saves are succesfull
     */
    public static function saveSettings($user_id, $values);

    /**
     * Removes a setting assigned to the user
     * @param mixed $user_id
     * @param string $key
     * @return boolean whether the delete is succesfull
     */
    public static function deleteSetting($user_id, $key);

    /**
     * Removes all user settings
     * @param mixed $user_id
     * @return boolean whether the delete is succesfull
     */
    public static function deleteSettings($user_id);
}
